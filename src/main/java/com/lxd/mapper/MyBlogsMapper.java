package com.lxd.mapper;

import com.lxd.po.Blog;
import com.lxd.po.Tag;
import com.lxd.po.Type;
import com.lxd.po.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
public interface MyBlogsMapper {

    /**
     * 根据当前用户查找博文
     * @param userId
     * @param typeIds
     * @param tagIds
     * @param text
     * @return
     */
    List<Blog> getContentByUser(String userId,String text,String typeIds,String tagIds);

    /**
     * 根据当前用户查找类型
     * @param userId
     * @return
     */
    @Select("select * from t_type where user_id = #{userId}")
    List<Type> getTypeByUser(Integer userId);

    /**
     * 根据当前用户查找类型
     * @param userId
     * @return
     */
    @Select("select * from t_tag where user_id = #{userId}")
    List<Tag> getTagByUser(Integer userId);
}
