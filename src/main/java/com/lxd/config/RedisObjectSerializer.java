package com.lxd.config;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

/**
 * com.lxd.util
 * Date： 2021/12/9 7:51 下午
 * User： cris
 * Description：
 **/
public class RedisObjectSerializer implements RedisSerializer {
    private Converter<Object, byte[]> serializingConverter = new SerializingConverter();
    private Converter<byte[], Object> deserializingConverter = new DeserializingConverter();
    private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];

    @Override
    public byte[] serialize(Object o) throws SerializationException {
        if (o == null) {    // 这个时候没有要序列化的对象出现，所以返回的字节数组应该就是一个空数组
            return EMPTY_BYTE_ARRAY ;
        }
        return this.serializingConverter.convert(o);
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        if (bytes == null || bytes.length == 0) {    // 此时没有对象的内容信息
            return null ;
        }
        return this.deserializingConverter.convert(bytes);
    }
}
