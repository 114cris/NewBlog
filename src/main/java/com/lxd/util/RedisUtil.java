package com.lxd.util;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * com.lxd.util
 * Date： 2021/12/9 8:10 下午
 * User： cris
 * Description： redis日常操作工具类
 **/
@Component
public class RedisUtil {
    @Autowired
    private StringRedisTemplate template;

    //存入数据
    public <T> void put(String key, T obj){
        template.opsForValue().set(key, JSON.toJSONString(obj));
    }

    //获取数据
    public String get(String key){
        return template.opsForValue().get(key);
    }

    //存入hash数据    例：hset  user  name    zhangsan
    public void putHash(String key, String value_key, Object value_value){
        template.opsForHash().put(key, value_key, value_value.toString());
    }

    //存入hash数据    例：hget  user  name
    public <T> Object getHash(String key, T obj){
        return template.opsForHash().get(key,String.valueOf(obj));
    }


}
