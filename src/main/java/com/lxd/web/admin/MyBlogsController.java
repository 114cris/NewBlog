package com.lxd.web.admin;

import com.lxd.po.User;
import com.lxd.service.MyBlogsService;
import com.lxd.util.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Author: Cris
 * Date: 2021/08/13
 * Time: 16:14
 * Project: demo
 * Description：
 **/
@RequestMapping("/admin")
@Controller
public class MyBlogsController {

    @Autowired
    private MyBlogsService myBlogsService;

    @GetMapping("/myBlogs")
    public String myBlogs(Model model, @CurrentUser User user){
//        mav.setViewName("/admin/myblogs");
        model.addAttribute("username",user.getUsername());
        model.addAttribute("userId",user.getId());
        model.addAttribute("types",myBlogsService.getTypeByuser(user.getId().intValue()));
        model.addAttribute("tags",myBlogsService.getTagByuser(user.getId().intValue()));
        return "admin/myBlogs";
    }

    @ResponseBody
    @RequestMapping("/getContentByUser")
    public List getContentByUser(String userId,String text,String typeIds,String tagIds){
        List list = myBlogsService.getContentByUser(userId,text,typeIds,tagIds);
        return list;
    }
}