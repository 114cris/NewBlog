package com.lxd.service;

import com.lxd.mapper.MyBlogsMapper;
import com.lxd.po.Blog;
import com.lxd.po.Tag;
import com.lxd.po.Type;
import com.lxd.po.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class MyBlogsServiceImpl implements MyBlogsService {

    @Autowired
    private MyBlogsMapper myBlogsMapper;

    /**
     * 根据当前用户查找博文
     * @param userId
     * @param typeIds
     * @param tagIds
     * @param text
     * @return
     */
    @Override
    public List<Blog> getContentByUser(String userId,String text,String typeIds,String tagIds) {
        List<Blog> contentByUser = myBlogsMapper.getContentByUser(userId,text,typeIds,tagIds);
        return contentByUser;
    }

    /**
     * 根据当前用户查找类型
     *
     * @param userId
     * @return
     */
    @Override
    public List<Type> getTypeByuser(Integer userId) {
        return myBlogsMapper.getTypeByUser(userId);
    }

    /**
     * 根据当前用户查找类型
     *
     * @param userId
     * @return
     */
    @Override
    public List<Tag> getTagByuser(Integer userId) {
        return myBlogsMapper.getTagByUser(userId);
    }
}
