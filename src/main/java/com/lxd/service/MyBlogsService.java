package com.lxd.service;

import com.lxd.po.Blog;
import com.lxd.po.Tag;
import com.lxd.po.Type;
import com.lxd.po.User;

import java.util.List;

public interface MyBlogsService {

    /**
     * 根据当前用户查找博文
     * @param userId
     * @param text
     * @param typeIds
     * @param tagIds
     * @return
     */
    List<Blog> getContentByUser( String userId,String text,String typeIds,String tagIds);

    /**
     * 根据当前用户查找类型
     * @param userId
     * @return
     */
    List<Type> getTypeByuser(Integer userId);

    /**
     * 根据当前用户查找类型
     * @param userId
     * @return
     */
    List<Tag> getTagByuser(Integer userId);
}
